﻿namespace ExplorerLike {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.treeViewFoldersAndFiles = new System.Windows.Forms.TreeView();
            this.imageListForTree = new System.Windows.Forms.ImageList(this.components);
            this.cboDrives = new System.Windows.Forms.ComboBox();
            this.cboFileTypes = new System.Windows.Forms.ComboBox();
            this.lblDriveInfo = new System.Windows.Forms.Label();
            this.lblFolderOrFileName = new System.Windows.Forms.Label();
            this.lblFileCountOrFileSize = new System.Windows.Forms.Label();
            this.lblFolderOrFileAttributes = new System.Windows.Forms.Label();
            this.lblFileTypes = new System.Windows.Forms.Label();
            this.lineDiv = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please select drive:";
            // 
            // treeViewFoldersAndFiles
            // 
            this.treeViewFoldersAndFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeViewFoldersAndFiles.ImageIndex = 0;
            this.treeViewFoldersAndFiles.ImageList = this.imageListForTree;
            this.treeViewFoldersAndFiles.Location = new System.Drawing.Point(15, 105);
            this.treeViewFoldersAndFiles.Name = "treeViewFoldersAndFiles";
            this.treeViewFoldersAndFiles.SelectedImageIndex = 0;
            this.treeViewFoldersAndFiles.Size = new System.Drawing.Size(554, 281);
            this.treeViewFoldersAndFiles.TabIndex = 6;
            this.treeViewFoldersAndFiles.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeViewFoldersAndFiles_BeforeExpand);
            this.treeViewFoldersAndFiles.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewFoldersAndFiles_AfterSelect);
            this.treeViewFoldersAndFiles.DoubleClick += new System.EventHandler(this.treeViewFoldersAndFiles_DoubleClick);
            // 
            // imageListForTree
            // 
            this.imageListForTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListForTree.ImageStream")));
            this.imageListForTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListForTree.Images.SetKeyName(0, "Folder_16x.png");
            this.imageListForTree.Images.SetKeyName(1, "Document_16x.png");
            // 
            // cboDrives
            // 
            this.cboDrives.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboDrives.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDrives.FormattingEnabled = true;
            this.cboDrives.Location = new System.Drawing.Point(145, 12);
            this.cboDrives.Name = "cboDrives";
            this.cboDrives.Size = new System.Drawing.Size(424, 24);
            this.cboDrives.TabIndex = 1;
            this.cboDrives.SelectedIndexChanged += new System.EventHandler(this.cboDrives_SelectedIndexChanged);
            // 
            // cboFileTypes
            // 
            this.cboFileTypes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboFileTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFileTypes.FormattingEnabled = true;
            this.cboFileTypes.Location = new System.Drawing.Point(145, 75);
            this.cboFileTypes.Name = "cboFileTypes";
            this.cboFileTypes.Size = new System.Drawing.Size(424, 24);
            this.cboFileTypes.Sorted = true;
            this.cboFileTypes.TabIndex = 5;
            this.cboFileTypes.SelectedIndexChanged += new System.EventHandler(this.cboFileTypes_SelectedIndexChanged);
            // 
            // lblDriveInfo
            // 
            this.lblDriveInfo.AutoSize = true;
            this.lblDriveInfo.Location = new System.Drawing.Point(12, 45);
            this.lblDriveInfo.Name = "lblDriveInfo";
            this.lblDriveInfo.Size = new System.Drawing.Size(88, 17);
            this.lblDriveInfo.TabIndex = 2;
            this.lblDriveInfo.Text = "No drive info";
            // 
            // lblFolderOrFileName
            // 
            this.lblFolderOrFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFolderOrFileName.AutoSize = true;
            this.lblFolderOrFileName.Location = new System.Drawing.Point(12, 392);
            this.lblFolderOrFileName.Name = "lblFolderOrFileName";
            this.lblFolderOrFileName.Size = new System.Drawing.Size(123, 17);
            this.lblFolderOrFileName.TabIndex = 7;
            this.lblFolderOrFileName.Text = "FolderOrFileName";
            // 
            // lblFileCountOrFileSize
            // 
            this.lblFileCountOrFileSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFileCountOrFileSize.AutoSize = true;
            this.lblFileCountOrFileSize.Location = new System.Drawing.Point(12, 411);
            this.lblFileCountOrFileSize.Name = "lblFileCountOrFileSize";
            this.lblFileCountOrFileSize.Size = new System.Drawing.Size(132, 17);
            this.lblFileCountOrFileSize.TabIndex = 8;
            this.lblFileCountOrFileSize.Text = "FileCountOrFileSize";
            // 
            // lblFolderOrFileAttributes
            // 
            this.lblFolderOrFileAttributes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFolderOrFileAttributes.AutoSize = true;
            this.lblFolderOrFileAttributes.Location = new System.Drawing.Point(12, 430);
            this.lblFolderOrFileAttributes.Name = "lblFolderOrFileAttributes";
            this.lblFolderOrFileAttributes.Size = new System.Drawing.Size(146, 17);
            this.lblFolderOrFileAttributes.TabIndex = 9;
            this.lblFolderOrFileAttributes.Text = "FolderOrFileAttributes";
            // 
            // lblFileTypes
            // 
            this.lblFileTypes.AutoSize = true;
            this.lblFileTypes.Location = new System.Drawing.Point(41, 79);
            this.lblFileTypes.Name = "lblFileTypes";
            this.lblFileTypes.Size = new System.Drawing.Size(103, 17);
            this.lblFileTypes.TabIndex = 4;
            this.lblFileTypes.Text = "Filter file types:";
            this.lblFileTypes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lineDiv
            // 
            this.lineDiv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineDiv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lineDiv.Location = new System.Drawing.Point(-1, 69);
            this.lineDiv.Name = "lineDiv";
            this.lineDiv.Size = new System.Drawing.Size(582, 1);
            this.lineDiv.TabIndex = 3;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(494, 422);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 25);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpen.Location = new System.Drawing.Point(413, 422);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 25);
            this.btnOpen.TabIndex = 10;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(581, 454);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lineDiv);
            this.Controls.Add(this.lblFileTypes);
            this.Controls.Add(this.lblFolderOrFileAttributes);
            this.Controls.Add(this.lblFileCountOrFileSize);
            this.Controls.Add(this.lblFolderOrFileName);
            this.Controls.Add(this.lblDriveInfo);
            this.Controls.Add(this.cboFileTypes);
            this.Controls.Add(this.cboDrives);
            this.Controls.Add(this.treeViewFoldersAndFiles);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Example for File and Folder TreeView control";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView treeViewFoldersAndFiles;
        private System.Windows.Forms.ImageList imageListForTree;
        private System.Windows.Forms.ComboBox cboDrives;
        private System.Windows.Forms.ComboBox cboFileTypes;
        private System.Windows.Forms.Label lblDriveInfo;
        private System.Windows.Forms.Label lblFolderOrFileName;
        private System.Windows.Forms.Label lblFileCountOrFileSize;
        private System.Windows.Forms.Label lblFolderOrFileAttributes;
        private System.Windows.Forms.Label lblFileTypes;
        private System.Windows.Forms.Label lineDiv;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOpen;
    }
}

