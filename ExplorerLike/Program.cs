﻿using System;
using System.Windows.Forms;

namespace ExplorerLike {
    static class Program {
        /// <summary>
        /// The main entry point for the ExplorerLike application.
        /// Just testing
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
