﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ExplorerLike {
    public partial class MainForm : Form {
        const string NODE_DUMMY_TEXT = "<please wait>";
        const string ALL_FILE_TYPES_TEXT = "All files";

        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            foreach (DriveInfo d in DriveInfo.GetDrives())
                cboDrives.Items.Add(d.Name);
            cboDrives.SelectedIndex = 0;
            RefreshDriveInfo(cboDrives.Items[0].ToString());
        }
        private void RefreshDriveInfo(String driveLetter) {
            DriveInfo driveInfo = DriveInfo.GetDrives()
                .Where(d => d.Name == driveLetter).FirstOrDefault();
            if (driveInfo != null && driveInfo.IsReady)
                this.lblDriveInfo.Text = "Name: " + driveInfo.VolumeLabel + "; " +
                    "Free size: " + FormatBytes(driveInfo.AvailableFreeSpace) + "; " +
                    "Total size: " + FormatBytes(driveInfo.TotalSize);
            else
                this.lblDriveInfo.Text = "Error or drive " + driveLetter + " is not ready";
        }
        private string FormatBytes(long bytes) {
            if (bytes < 1024)
                return bytes.ToString() + " bytes";
            else if (bytes <= 1024 * 1024)
                return ((float)bytes / 1024).ToString("N2") + " kB";
            else if (bytes <= 1024 * 1024 * 1024)
                return ((float)bytes / (1024 * 1024)).ToString("N2") + " MB";
            else
                return ((float)bytes / (1024 * 1024 * 1024)).ToString("N2") + " GB";
        }

        private void PopulateTreeView(String drive) {
            SetVisibleFolderAndFilesControls(false);
            cboFileTypes.Items.Clear();
            cboFileTypes.Items.Add(ALL_FILE_TYPES_TEXT);
            cboFileTypes.SelectedIndex = 0;

            TreeNode rootNode;
            treeViewFoldersAndFiles.Nodes.Clear();
            DirectoryInfo dirInfo = new DirectoryInfo(drive);
            if (dirInfo.Exists) {
                rootNode = new TreeNode(dirInfo.Name, 0, 0);
                rootNode.Tag = "root";
                RefreshNode(dirInfo.FullName, rootNode);
                treeViewFoldersAndFiles.Nodes.Add(rootNode);
                treeViewFoldersAndFiles.SelectedNode = rootNode;
                rootNode.Expand();
            }
            RefreshFolderOrFileInfo();
            SetVisibleFolderAndFilesControls(true);
        }
        private void SetVisibleFolderAndFilesControls(Boolean visible) {
            lblFileTypes.Visible = visible;
            cboFileTypes.Visible = visible;
            treeViewFoldersAndFiles.Visible = visible;
            lblFolderOrFileName.Visible = visible;
            lblFileCountOrFileSize.Visible = visible;
            lblFolderOrFileAttributes.Visible = visible;
        }

        private void RefreshNode(String fullPath, TreeNode nodeToRefresh) {
            DirectoryInfo dirInfo = new DirectoryInfo(fullPath);
            nodeToRefresh.Nodes.Clear();
            if (dirInfo.Exists) {
                foreach (DirectoryInfo subDir in GetDirectories(dirInfo)) {
                    TreeNode dirNode = new TreeNode(subDir.Name, 0, 0);
                    if (GetDirectories(subDir).Length != 0 || GetFiles(subDir).Length != 0)
                        dirNode.Nodes.Add(NODE_DUMMY_TEXT);
                    nodeToRefresh.Nodes.Add(dirNode);
                }
                foreach (FileInfo file in GetFiles(dirInfo)) {
                    if (!cboFileTypes.Items.Contains(file.Extension.ToLower()))
                        cboFileTypes.Items.Add(file.Extension.ToLower());
                    if (cboFileTypes.SelectedIndex > 0 &&
                          (cboFileTypes.Items[cboFileTypes.SelectedIndex].ToString() == ALL_FILE_TYPES_TEXT ||
                          file.Extension.ToLower() == cboFileTypes.Items[cboFileTypes.SelectedIndex].ToString().ToLower())) {
                        // Only add item if not filtered (all files) or extension match filter
                        TreeNode fileNode = new TreeNode(file.Name, 1, 1);
                        fileNode.Tag = "file";
                        nodeToRefresh.Nodes.Add(fileNode);
                    }
                }
            }
        }

        private DirectoryInfo[] GetDirectories(DirectoryInfo dirInfo) {
            DirectoryInfo[] result = new DirectoryInfo[0];
            var t = Task.Run(() => {
                try {
                    if (dirInfo.Exists)
                        result = dirInfo.GetDirectories();
                }
                catch (UnauthorizedAccessException) {
                    // GetDirectories() can throw error for access denied and I ignore it
                }
            });
            t.Wait();
            return result;
        }

        private FileInfo[] GetFiles(DirectoryInfo dirInfo) {
            FileInfo[] result = new FileInfo[0];
            var t = Task.Run(() => {
                try {
                    if (dirInfo.Exists)
                        result = dirInfo.GetFiles();
                }
                catch (UnauthorizedAccessException) {
                    // GetFiles() can throw error for access denied and I ignore it
                }
            });
            t.Wait();
            return result;
        }

        private void treeViewFoldersAndFiles_BeforeExpand(object sender, TreeViewCancelEventArgs e) {
            if (e.Node.FirstNode.Text == NODE_DUMMY_TEXT)
                RefreshNode(e.Node.FullPath, e.Node);
        }

        private void cboDrives_SelectedIndexChanged(object sender, EventArgs e) {
            String selectedDrive = cboDrives.SelectedItem.ToString();
            lblDriveInfo.Text = "Please wait, reading drive " + selectedDrive + " info...";
            PopulateTreeView(selectedDrive);
            RefreshDriveInfo(selectedDrive);
        }

        private void treeViewFoldersAndFiles_AfterSelect(object sender, TreeViewEventArgs e) {
            RefreshFolderOrFileInfo();
        }
        private void RefreshFolderOrFileInfo() {
            if (treeViewFoldersAndFiles.SelectedNode != null) {
                if (IsFileNodeSelected())
                    RefreshFileInfo();
                else
                    RefreshFolderInfo();
            }
            else {
                lblFolderOrFileName.Text = "";
                lblFileCountOrFileSize.Text = "";
                lblFolderOrFileAttributes.Text = "";
            }
        }
        private void RefreshFileInfo() {
            lblFolderOrFileName.Text = "File name: " + treeViewFoldersAndFiles.SelectedNode.Text;
            FileInfo fileInfo = new FileInfo(treeViewFoldersAndFiles.SelectedNode.FullPath);
            if (fileInfo.Exists)
                lblFileCountOrFileSize.Text = "File size: " + FormatBytes(fileInfo.Length);
            else
                lblFileCountOrFileSize.Text = "File not found";
            lblFolderOrFileAttributes.Text = "Attributes: " + fileInfo.Attributes.ToString();
        }
        private void RefreshFolderInfo() {
            lblFolderOrFileName.Text = "Folder name: " + treeViewFoldersAndFiles.SelectedNode.Text;
            DirectoryInfo dirInfo = new DirectoryInfo(treeViewFoldersAndFiles.SelectedNode.FullPath);
            if (dirInfo.Exists) {
                FileInfo[] files = GetFiles(dirInfo);
                lblFileCountOrFileSize.Text = "Number of files: " +
                    (cboFileTypes.Items[cboFileTypes.SelectedIndex].ToString() == ALL_FILE_TYPES_TEXT ? "" :
                    files.Count(f => f.Extension.ToLower() == cboFileTypes.Items[cboFileTypes.SelectedIndex].ToString().ToLower()).ToString() + "/") +
                    files.Length.ToString() + " file(s) found";
            }
            else
                lblFileCountOrFileSize.Text = "0 file(s) found";
            lblFolderOrFileAttributes.Text = "Attributes: " + dirInfo.Attributes.ToString();
        }
        private Boolean IsFileNodeSelected() {
            return treeViewFoldersAndFiles.SelectedNode.Tag != null &&
                    treeViewFoldersAndFiles.SelectedNode.Tag.ToString() == "file";
        }

        private void treeViewFoldersAndFiles_DoubleClick(object sender, EventArgs e) {
            OpenSelectedFolderOrFile();
        }
        private void OpenSelectedFolderOrFile() {
            Process.Start(treeViewFoldersAndFiles.SelectedNode.FullPath);
        }

        private void cboFileTypes_SelectedIndexChanged(object sender, EventArgs e) {
            if (treeViewFoldersAndFiles.Nodes.Count > 0) {
                RefreshNode(cboDrives.SelectedItem.ToString(), treeViewFoldersAndFiles.Nodes[0]);
                RefreshFolderOrFileInfo();
            }
        }

        private void btnOpen_Click(object sender, EventArgs e) {
            OpenSelectedFolderOrFile();
        }

        private void btnClose_Click(object sender, EventArgs e) {
            Close();
        }
    }
}

